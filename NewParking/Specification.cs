﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParking
{
    class SpecParking
    {
        /// <summary>
        /// Цена за час
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Минимальный чек за день
        /// </summary>
        public decimal MinPrice { get; set; }

        /// <summary>
        /// Минимальное время
        /// </summary>
        public decimal MinTime { get; set; }

        /// <summary>
        /// Стоимость парковки за день
        /// </summary>
        public decimal DayPrice { get; set; }

        /// <summary>
        /// Счастливые часы
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Размер суммарной скилки на чек
        /// </summary>
        public decimal HappyDay { get; set; }

        /// <summary>
        /// Реклама
        /// </summary>
        public string Advert { get; set; }

        /// <summary>
        /// День рождения хозяина
        /// </summary>
        public DateTime OwnersBirthDay { get; set; }  
    }
}
