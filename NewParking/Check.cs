﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParking
{
    class Check
    {
        private static decimal _price { get; set; }
        private static decimal _minPrice { get; set; }
        private static decimal _minTime { get; set; }
        private static decimal _dayPrice { get; set; }
        private static decimal _discount { get; set; }
        private static string _advert { get; set; }
        private static DateTime _start;
        private static DateTime _end;
        private static DateTime _ownersBD;
        private static decimal _happy;

        /// <summary>
        /// В конструкторе использованы тестовые данные
        /// _start - въезд на парковку
        /// _end - выезд
        /// </summary>
        /// <param name="spec">Спецификация передаваемая в качестве параметра</param>
        public Check(SpecParking spec)
        {
            _price = spec.Price;
            _minPrice = spec.MinPrice;
            _dayPrice = spec.DayPrice;
            _minTime = spec.MinTime;
            _discount = spec.Discount;
            _advert = spec.Advert;
            _happy = spec.HappyDay;
            _ownersBD = spec.OwnersBirthDay;

            _start = new DateTime(2017, 03, 23, 22, 00, 00);
            _end = new DateTime(2017, 03, 25, 23, 00, 00);

            PrintCheck();
        }

        /// <summary>
        /// Метод считает количество часов в чеке
        /// </summary>
        /// <returns>Кол-во часов</returns>
        private int Time()
        {
            var result = _end.Hour - _start.Hour;
            return result < 0 ? result + 24 : result;
        }

        /// <summary>
        /// Метод считает количество дней в чеке
        /// </summary>
        /// <returns>Кол-во дней</returns>
        private int Days()
        {
            var result = _end.DayOfYear - _start.DayOfYear;

            if (_end.Hour - _start.Hour < 0) result = result - 1; 

            return result;
        }

        /// <summary>
        /// Скидка на весь чек в счасливые дни
        /// </summary>
        /// <returns>Коэффициент для счастливых дней</returns>
        private decimal SummuryDiscount()
        {
            if (_end.DayOfWeek == DayOfWeek.Saturday
                || _end.DayOfWeek == DayOfWeek.Sunday
                || (_end.Month == _ownersBD.Month && _end.Day == _ownersBD.Day)
                ) return _happy;
            else return 1;    
        }

        /// <summary>
        /// Метод высчитывает сумму для оплаты
        /// </summary>
        /// <returns>Сумму к оплате</returns>
        private decimal Payment()
        {
            var result = default(decimal);

            result += Days() * _dayPrice;

            _start = _start.AddDays(Days());

            var temp = default(decimal);

            while (_start.CompareTo(_end) < 0)
            {
                temp += _price * Discount();
                _start = _start.AddHours(1);
            }

            if (temp < _minPrice)
                temp = _minPrice;

            return (result + temp) * SummuryDiscount();
        }
        
        /// <summary>
        /// Метод в котором записаны счастливые часы
        /// </summary>
        /// <returns>Коэфициент на который будет умножаться каждый час,
        /// после проверки на совпадение</returns>
        private decimal Discount()
        {
            if (_start.Hour == 22 || _start.Hour == 23 || _start.Hour == 0 || _start.Hour == 1 || _start.Hour == 2 ||
                _start.Hour == 3 || _start.Hour == 4 || _start.Hour == 5 || _start.Hour == 6)
                return _discount;
            else return 1;
        }

        /// <summary>
        /// Метод выводящий на консоль чек
        /// </summary>
        private void PrintCheck()
        {
            var lst = new List<string>();

            lst.Add("Hello!");
            lst.Add($"CheckNumber: {_start:s}");
            lst.Add($"MinPrice is {_minPrice:C}");
            lst.Add($"Price is {_price:C}");
            lst.Add($"DayPrice is {_dayPrice:C}");
            lst.Add($"Days: {Days():00} days; Time: {Time():00} hour(s)");
            lst.Add($"To pay {Payment():C}");
            lst.Add(string.Empty);
            lst.Add("Place for advertising:");
            lst.Add(_advert);

            foreach (var str in lst)
                Console.WriteLine(str);
        }
    }
}
